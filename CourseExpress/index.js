let arr = ["one", "two", "three"];
const { response } = require("express");
const express = require("express"); //Подключаем express
const RouterForArr = express.Router(); // подключаем возможность создания маршрутов Урок 9

const app = express(); // Вызываем экспресс и записываем его в объект. Теперь данный объект имеет кучу методов

// Урок 15 Промежуточные методы. Реализаця должна быть выше маршрутов
// Можно менять данные в запросе а так же точечно работать с необходимыми
app.use((req, res, next) => {
  console.log(
    "Date",
    new Date(),
    "Method",
    req.method,
    "URL",
    req.originalUrl,
    "IP",
    req.ip
  );
  // В консоли будет информация про все запросы, при всех вводимых маршрутах
  next();
});

// Урок 4.
//req - данные отправленные от клиента (HTTP запрос)
//res - помогают в отправке данных клиенту (HTTP отклик)
//next -
app.get("/", (req, res, next) => {
  res.send("Сообщение пользователю: Все работает!!!");
});

// Урок 5. просто отправка
// По маршруту отправляем массив
app.get("/just-send-data", (req, res, next) => {
  res.send(arr);
});

// Урок 6. отправка данных в json
// По маршруту отправляем json
app.get("/send-json-data", (req, res, next) => {
  res.json({ arr });
});

// Урок 7. query параметры в юрл
// Получаем данные со стороны клиента через query(req.query) параметры
// параметр localhost:5000/test-query?page=0 (page - ключ а 0 значение)
app.get("/test-query", (req, res, next) => {
  console.log("Данные: ", req.query);
});

// Урок 8. Статусы ошибок
//
app.get("/test-query/:id", (req, res, next) => {
  if (arr[req.params.id]) {
    res.send(arr[req.params.id]);
  } else {
    res.status(404).send("Error"); // Отправка ошибки
  }
});

// Урок 9. Использование собственного маршрута express.Router() дополнительно к маршрутам через app
//Нужно это все для модульности и для простоты создания маршрутов
RouterForArr.get("/", (req, res) => {
  res.send("/ - Использование такого маршрута");
});

RouterForArr.get("/ArrNext", (req, res) => {
  res.send("/Arr - Использование такого маршрута");
});

app.use("/Arr", RouterForArr);

// Урок 10. Параметры маршрута. Получение некоторых сущностей по id
// req.params.id - при запросе http, в объекте params есть наши ид
app.get("/arr/:id", (req, res) => {
  res.send(arr[req.params.id]);
});

// Урок 11. Редирект
// Просто на сайт
app.get("/test-redirect-on-site", (req, res) => {
  res.redirect("https://knastu.ru/");
});

// Просто на роутер
app.get("/test-redirect-on-router", (req, res) => {
  res.redirect("/arr/2");
  // res.redirect(301, "/arr/2"); 301-еще можно и статус отправить
});

// Урок 12.Отправка файла
// Первый аргумент это путь, второй это название файла при скачивании, третий коллбек вызванная при успешной отправке файла
app.get("/downloadImg", (req, res) => {
  res.download("./public/1.jpg");
});

// Урок 13. Статические файлы. Файлы которые необходимые для отображения на клиенте
// express.static() - метод который может работать с такой папкой статических файлов
// __dirname полный путь до папки
app.use("/static", express.static(__dirname + "/public"));
// app.use(express.static("/public")); - без собственного пути

// Урок 14. Они могут заранее просматривать результат запросов
app.get("/testNext", (req, res, next) => {
  next(); // без функции некст при запросе страница будет висеть
});

// Урок 16. Промежуточные методы для обработки ошибок
// Нужно вызывать его после всех app.use и маршрутов
app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).send(err.stack);
});

// Урок 3.
//передаем номер порта, который прослушивается функцией и метод обратного вызова,
//который запускает функцию в случае удачного запуска
app.listen(5000, () => {
  console.log("Запущен", new Date());
});
